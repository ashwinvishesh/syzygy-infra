package com.lifing.syzygy.infra.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.lifing.syzygy.infra.document.SyCounter;

@Component("sequence")
public class SyMongoSequence {

	@Autowired
	private MongoOperations mongoOperation;

	public Integer getNextSequence(String key) {

		// get sequence id
		if (null == key) {
			key = "randomId";
		}
		Query query = new Query(Criteria.where("_id").is(key+"Id"));

		// increase sequence id by 1
		Update update = new Update();
		update.inc("seq", 1);

		// return new increased id
		FindAndModifyOptions options = new FindAndModifyOptions();
		options.returnNew(true);

		// this is the magic happened.
		SyCounter seqId = mongoOperation.findAndModify(query, update, options, SyCounter.class);

		// if no id, throws SequenceException
		// optional, just a way to tell user when the sequence id is failed to
		// generate.
		if (seqId == null) {
			return 0;
		}

		return seqId.getSeq();

	}
}
