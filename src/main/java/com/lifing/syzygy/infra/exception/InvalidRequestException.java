package com.lifing.syzygy.infra.exception;

import org.springframework.validation.Errors;

@SuppressWarnings("serial")
public class InvalidRequestException extends SyzBaseException {
	private Errors errors;

	public InvalidRequestException(String message, Errors errors) {
		super(message);
		this.errors = errors;
	}

	public InvalidRequestException(String message) {
		super(message);
	}

	public Errors getErrors() {
		return errors;
	}
}