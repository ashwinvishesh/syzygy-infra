package com.lifing.syzygy.infra.file.operation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.Document;
import org.springframework.stereotype.Component;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.prefs.CsvPreference;

@Component
public class CsvProcessor {

	public void generate(List<Document> profiles, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (profiles.size() != 0) {

				response.setContentType("text/csv");
				response.setHeader("Content-Disposition", "attachment; filename=\""
						+ new SimpleDateFormat("dd.MM.yyyy.HH.mm.ss").format(new Date()) + ".csv\"");
				response.setCharacterEncoding("UTF-8");

				CsvMapWriter csvWriter;

				Document doc = profiles.get(0);
				Map<String, Object> docProf = new LinkedHashMap<String, Object>();
				if (doc.containsKey("paid")) {
					docProf.put("transactionId", UUID.randomUUID().toString());
					docProf.put("upiId", doc.get("upiId"));
					docProf.put("name", doc.get("name"));
					docProf.put("email", doc.get("email"));
					docProf.put("phoneNumber", doc.get("phoneNumber"));
					docProf.put("paid", doc.get("paid"));
				} else {
					docProf.putAll(doc);
				}

				List<String> header = new ArrayList<String>();

				for (Map.Entry<String, Object> entry : docProf.entrySet()) {
					if (entry.getKey().equals("education")) {
						header.add("graduationMonth");
						header.add("college");
						header.add("graduationYear");
						header.add("course");
					}
					if (entry.getKey().equals("location")) {
						header.add("location");
					} else {
						header.add(entry.getKey());
					}
				}
				header.remove("_id");
				header.remove("education");
				csvWriter = new CsvMapWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
				csvWriter.writeHeader(header.stream().toArray(String[]::new));

				for (Document profile : profiles) {
					if (profile.containsKey("paid")) {
						// Inserting transactionId for profiles
						profile.put("transactionId", UUID.randomUUID().toString());
					}

					// write data to map and map to csvWriter
					Map<String, Object> profileDoc = new HashMap<String, Object>();
					profileDoc.putAll(profile);
					profileDoc.remove("_id");
					if (profile.containsKey("education")) {
						Document innerProfile = (Document) profile.get("education");
						profileDoc.put("graduationMonth", innerProfile.get("graduationMonth"));
						profileDoc.put("college", innerProfile.get("college"));
						profileDoc.put("graduationYear", innerProfile.get("graduationYear"));
						profileDoc.put("course", innerProfile.get("course"));
						profileDoc.remove("education");
					}
					if (profile.containsKey("location")) {
						profileDoc.put("location", ((Document) profile.get("location")).get("name"));
					}

					csvWriter.write(profileDoc, header.stream().toArray(String[]::new));
				}

				csvWriter.close();

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
