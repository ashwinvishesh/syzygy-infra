package com.lifing.syzygy.infra.util;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.bson.Document;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
public class RemoteService {

	//@Autowired
	//private EurekaClient eurekaClient;

	private final RestTemplate restTemplate;

	public RemoteService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	public String getRemoteUrl(String applicationName, String apiPath) {
		String url = null;
		try {
			if (applicationName.contains("educationhackfest") || applicationName.contains("lemonop")
					|| applicationName.contains("localhost")) {
				url = applicationName + apiPath;
			}/* else {
				Application application = eurekaClient.getApplication(applicationName);
				InstanceInfo instanceInfo = application.getInstances().get(0);
				url = "http://" + instanceInfo.getHostName() + ":" + instanceInfo.getPort() + apiPath;
			}*/
		} catch (Exception e) {

		}
		return url;
	}

	public HttpHeaders getHeader(String token) {
		HttpHeaders headers = new HttpHeaders();
		try {
			if (null == token) {
				HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
						.getRequest();
				headers.add("X-Access-Token", request.getHeader("X-Access-Token"));
			} else {
				headers.add("X-Access-Token", token);
			}
		} catch (Exception e) {

		}
		return headers;
	}

	public Document invokeGetService(String applicationName, String apiPath, Document data) {
		try {

			String url = getRemoteUrl(applicationName, apiPath);

			HttpHeaders headers = getHeader(null);

			HttpEntity<Map<String, Object>> entity = null;
			if (null == data)
				entity = new HttpEntity<>(headers);
			else
				entity = new HttpEntity<>(data, headers);

			restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

			ResponseEntity<Document> response = restTemplate.exchange(url, HttpMethod.GET, entity, Document.class);
			return response.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Document invokeGetService(String applicationName, String apiPath, Document data, String token) {
		try {

			String url = getRemoteUrl(applicationName, apiPath);

			HttpHeaders headers = getHeader(token);

			HttpEntity<Map<String, Object>> entity = null;
			if (null == data)
				entity = new HttpEntity<>(headers);
			else
				entity = new HttpEntity<>(data, headers);

			restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

			ResponseEntity<Document> response = restTemplate.exchange(url, HttpMethod.GET, entity, Document.class);
			return response.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Document invokePostService(String applicationName, String apiPath, Document data) {
		try {
			String url = getRemoteUrl(applicationName, apiPath);

			HttpHeaders headers = getHeader(null);

			HttpEntity<Map<String, Object>> entity = new HttpEntity<>(data, headers);
			restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

			ResponseEntity<Document> response = restTemplate.exchange(url, HttpMethod.POST, entity, Document.class);
			return response.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Document invokePostService(String applicationName, String apiPath, Document data, String token) {
		try {
			String url = getRemoteUrl(applicationName, apiPath);

			HttpHeaders headers = getHeader(token);

			HttpEntity<Map<String, Object>> entity = new HttpEntity<>(data, headers);
			restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

			ResponseEntity<Document> response = restTemplate.exchange(url, HttpMethod.POST, entity, Document.class);
			return response.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Document invokePutService(String applicationName, String apiPath, Document data, String token) {
		try {
			String url = getRemoteUrl(applicationName, apiPath);

			HttpHeaders headers = getHeader(token);

			HttpEntity<Map<String, Object>> entity = new HttpEntity<>(data, headers);

			restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

			ResponseEntity<Document> response = restTemplate.exchange(url, HttpMethod.PUT, entity, Document.class);
			return response.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Document invokePatchService(String applicationName, String apiPath, Document data) {
		try {
			String url = getRemoteUrl(applicationName, apiPath);

			HttpHeaders headers = getHeader(null);

			HttpEntity<Map<String, Object>> entity = new HttpEntity<>(data, headers);

			restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

			ResponseEntity<Document> response = restTemplate.exchange(url, HttpMethod.PATCH, entity, Document.class);
			return response.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Object invokeService(String applicationName, String apiPath, Document data, HttpMethod method,
			String returnType) {
		try {
			String url = getRemoteUrl(applicationName, apiPath);

			HttpHeaders headers = getHeader(null);

			HttpEntity<Map<String, Object>> entity = null;
			if (null == data)
				entity = new HttpEntity<>(headers);
			else
				entity = new HttpEntity<>(data, headers);

			restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

			if (returnType.equals("DOC")) {
				ResponseEntity<Document> response = restTemplate.exchange(url, method, entity, Document.class);
				return response.getBody();
			} else {
				ResponseEntity<List> response = restTemplate.exchange(url, method, entity, List.class);
				return response.getBody();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
