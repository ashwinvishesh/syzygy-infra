package com.lifing.syzygy.infra.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

public class GigTitleGenerator {
	private final static int LENGTH = 21;
	private final static int LINES = 18;
	
	private GigTitleGenerator() {
	}
	
	public static void generateGigTitleArray() {
		try {
			BufferedReader gigTitleData = new BufferedReader(new FileReader(new File("/Users/swaroopc/Swar/Syzygy/gigsdb.csv")));
			
			String [] output = new String[LENGTH];
			for (int i = 0; i < LENGTH; i++) {
				output[i] = "vm.dataset" + (i + 1) + " = [";
			}
			
			String line = gigTitleData.readLine();
			int lineCount = 1;
			while(null != line) {
				StringTokenizer tokens = new StringTokenizer(line, ",");
				int index = 0;
				while(tokens.hasMoreTokens()) {
					if (lineCount == LINES)
						output[index] = output[index] + "\"" + tokens.nextToken() + "\"";
					else 
						output[index] = output[index] + "\"" + tokens.nextToken() + "\", ";
					index++;
				}
				lineCount++;
				line = gigTitleData.readLine();
			}
			
			gigTitleData.close();
			
			for (int i = 0; i < LENGTH; i++) {
				output[i] = output[i] + "];";
				System.out.println(output[i]);
				System.out.println();
			}
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}
		finally {
			
		}
	}
	
	public static void main(String args[]) {
		GigTitleGenerator.generateGigTitleArray();
	}
}
